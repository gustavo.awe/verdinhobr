﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VerdinhoBR.Dados;
using VerdinhoBR.Models;

namespace VerdinhoBR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdmsController : ControllerBase
    {
        private readonly UsuarioContext _context;

        public AdmsController(UsuarioContext context)
        {
            _context = context;
        }

        // GET: api/Adms
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Adm>>> GetAdms()
        {
            return await _context.Adms.ToListAsync();
        }

        // GET: api/Adms/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Adm>> GetAdm(int id)
        {
            var adm = await _context.Adms.FindAsync(id);

            if (adm == null)
            {
                return NotFound();
            }

            return adm;
        }

        // PUT: api/Adms/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAdm(int id, Adm adm)
        {
            if (id != adm.Id)
            {
                return BadRequest();
            }

            _context.Entry(adm).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AdmExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Adms
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Adm>> PostAdm(Adm adm)
        {
            _context.Adms.Add(adm);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAdm", new { id = adm.Id }, adm);
        }

        // DELETE: api/Adms/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Adm>> DeleteAdm(int id)
        {
            var adm = await _context.Adms.FindAsync(id);
            if (adm == null)
            {
                return NotFound();
            }

            _context.Adms.Remove(adm);
            await _context.SaveChangesAsync();

            return adm;
        }

        private bool AdmExists(int id)
        {
            return _context.Adms.Any(e => e.Id == id);
        }
    }
}
