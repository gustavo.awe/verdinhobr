﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VerdinhoBR.Dados;
using VerdinhoBR.Models;

namespace VerdinhoBR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostoesController : ControllerBase
    {
        private readonly UsuarioContext _context;

        public PostoesController(UsuarioContext context)
        {
            _context = context;
        }

        // GET: api/Postoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Posto>>> GetPosto()
        {
            return await _context.Posto.ToListAsync();
        }

        // GET: api/Postoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Posto>> GetPosto(int id)
        {
            var posto = await _context.Posto.FindAsync(id);

            if (posto == null)
            {
                return NotFound();
            }

            return posto;
        }

        // PUT: api/Postoes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPosto(int id, Posto posto)
        {
            if (id != posto.PostoId)
            {
                return BadRequest();
            }

            _context.Entry(posto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Postoes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Posto>> PostPosto(Posto posto)
        {
            _context.Posto.Add(posto);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPosto", new { id = posto.PostoId }, posto);
        }

        // DELETE: api/Postoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Posto>> DeletePosto(int id)
        {
            var posto = await _context.Posto.FindAsync(id);
            if (posto == null)
            {
                return NotFound();
            }

            _context.Posto.Remove(posto);
            await _context.SaveChangesAsync();

            return posto;
        }

        private bool PostoExists(int id)
        {
            return _context.Posto.Any(e => e.PostoId == id);
        }
    }
}
