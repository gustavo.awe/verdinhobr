﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VerdinhoBR.Dados;
using VerdinhoBR.Models;

namespace VerdinhoBR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServicoPagamentoesController : ControllerBase
    {
        private readonly UsuarioContext _context;

        public ServicoPagamentoesController(UsuarioContext context)
        {
            _context = context;
        }

        // GET: api/ServicoPagamentoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ServicoPagamento>>> GetServicoPagamento()
        {
            return await _context.ServicoPagamento.ToListAsync();
        }

        // GET: api/ServicoPagamentoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ServicoPagamento>> GetServicoPagamento(int id)
        {
            var servicoPagamento = await _context.ServicoPagamento.FindAsync(id);

            if (servicoPagamento == null)
            {
                return NotFound();
            }

            return servicoPagamento;
        }

        // PUT: api/ServicoPagamentoes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutServicoPagamento(int id, ServicoPagamento servicoPagamento)
        {
            if (id != servicoPagamento.ServicoPagamentoId)
            {
                return BadRequest();
            }

            _context.Entry(servicoPagamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServicoPagamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ServicoPagamentoes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ServicoPagamento>> PostServicoPagamento(ServicoPagamento servicoPagamento)
        {
            _context.ServicoPagamento.Add(servicoPagamento);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetServicoPagamento", new { id = servicoPagamento.ServicoPagamentoId }, servicoPagamento);
        }

        // DELETE: api/ServicoPagamentoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ServicoPagamento>> DeleteServicoPagamento(int id)
        {
            var servicoPagamento = await _context.ServicoPagamento.FindAsync(id);
            if (servicoPagamento == null)
            {
                return NotFound();
            }

            _context.ServicoPagamento.Remove(servicoPagamento);
            await _context.SaveChangesAsync();

            return servicoPagamento;
        }

        private bool ServicoPagamentoExists(int id)
        {
            return _context.ServicoPagamento.Any(e => e.ServicoPagamentoId == id);
        }
    }
}
