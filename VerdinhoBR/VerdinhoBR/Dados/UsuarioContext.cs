﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VerdinhoBR.Models;

namespace VerdinhoBR.Dados
{
    public class UsuarioContext : DbContext
    {
        public UsuarioContext(DbContextOptions<UsuarioContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Pagamento>()
                .HasKey(ac => new { ac.ClienteId, ac.FuncionarioId, ac.ServicoId });
        }

        public DbSet<Adm> Adms { get; set; }
        public DbSet<Funcionario> Funcionarios { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<VerdinhoBR.Models.Carteira> Carteira { get; set; }
        public DbSet<VerdinhoBR.Models.Pagamento> Pagamento { get; set; }
        public DbSet<VerdinhoBR.Models.Posto> Posto { get; set; }
        public DbSet<VerdinhoBR.Models.Servicos> Servicos { get; set; }
        public DbSet<VerdinhoBR.Models.ServicoPagamento> ServicoPagamento { get; set; }
        public DbSet<VerdinhoBR.Models.Usuario> Usuario { get; set; }
        public DbSet<VerdinhoBR.Models.Veiculo> Veiculo { get; set; }


    }
}
