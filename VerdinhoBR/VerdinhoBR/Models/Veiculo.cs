﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VerdinhoBR.Models
{
    public class Veiculo
    {
        public int VeiculoId { get; set; }
        public string Modelo { get; set; }
        public string Placa { get; set; }
        public Cliente Cliente { get; set; }
    }
}
