﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VerdinhoBR.Models
{
    public class Usuario
    {
        public string Nome { get; set; }
        public int Id { get; set; }
        public double Telefone { get; set; }
        public int CPF { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }

    }
}
