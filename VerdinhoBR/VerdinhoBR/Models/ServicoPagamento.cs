﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VerdinhoBR.Models
{
    public class ServicoPagamento
    {
        public int ServicoPagamentoId { get; set; }
        public int PagamentoId { get; set; }
        public int ServicoId { get; set; }

        public Pagamento Pagamento { get; set; }
        public Servicos Servicos { get; set; }
    }
}
