﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VerdinhoBR.Models
{
    public class Servicos 
    {
        public int ServicosId { get; set; }
        public string Tipo { get; set; }
        public double Preco { get; set; }
        public Pagamento Pagamentos { get; set; }
        
    }
}
