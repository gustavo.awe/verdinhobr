﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VerdinhoBR.Models
{
    public class Posto
    {
        public string Nome { get; set; }
        public string Localizacao { get; set; }
        public Adm Adm { get; set; }
        public int PostoId { get; set; }
    }
}
