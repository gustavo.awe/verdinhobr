﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VerdinhoBR.Models
{
    public class Carteira
    {
        public int CarteiraId { get; set; }
        public int PagamentoBoleto { get; set; }
        public int PagamentoCredito { get; set; }
        public int PagamentoDebito { get; set; }
        private bool ValidarPagamento { get; set; }
        public Cliente Cliente { get; set; }

    }
}
