﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VerdinhoBR.Models;

namespace VerdinhoBR.Models
{   
    public class Pagamento
    {
        public int ClienteId { get; set; }
        public int FuncionarioId { get; set; }
        public int ServicoId { get; set; }

        public Cliente Cliente { get; set; }
        public Funcionario Funcionario { get; set; }
        public List<Servicos>  Servicos { get; set; }
        
    }
}
